import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class MenuBox {
	private JFrame frame;
	private JPanel color;
	
	private JMenuBar menu;
	private JMenu colorList;
	private JMenuItem red;
	private JMenuItem green;
	private JMenuItem blue;
	public static void main(String[] args) {
		MenuBox n = new MenuBox();
		n.setFrame();
	}

	public void setFrame() {
		frame = new JFrame("MENU BOX");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);

		color = new JPanel(new BorderLayout());
		menu = new JMenuBar();
		frame.setJMenuBar(menu);
		colorList = new JMenu("COLOR");
		menu.add(colorList);
		red = new JMenuItem("RED");
		green = new JMenuItem("GREEN");
		blue = new JMenuItem("BLUE");
		colorList.add(red);
		colorList.add(green);
		colorList.add(blue);
		Listener lis = new Listener();
		setListener(lis);
		ButtonGroup group = new ButtonGroup();
		group.add(red);
		group.add(green);
		group.add(blue);
		frame.add(color);
		frame.setVisible(true);
	}

	public void setListener(ActionListener ac) {
		red.addActionListener(ac);
		green.addActionListener(ac);
		blue.addActionListener(ac);
	}

	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getActionCommand() == "RED") {
				color.setBackground(new Color(255, 0, 0));
			} else if (e.getActionCommand() == "GREEN") {
				color.setBackground(new Color(0, 255, 0));
			} else if (e.getActionCommand() == "BLUE") {
				color.setBackground(new Color(0, 0, 255));
			}
		}
	}
}
