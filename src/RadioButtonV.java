import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class RadioButtonV {
	private JFrame frame;
	private JPanel control;
	private JPanel color;
	
	private JRadioButton red;
	private JRadioButton green;
	private JRadioButton blue;
	public static void main(String[] args) {
		RadioButtonV n = new RadioButtonV();
		n.setFrame();
	}

	public void setFrame() {
		frame = new JFrame("RADIO BUTTON");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);

		color = new JPanel(new BorderLayout());
		control = new JPanel(new FlowLayout());
		control.setPreferredSize(new Dimension(100, 50));
		red = new JRadioButton("RED");
		green = new JRadioButton("GREEN");
		blue = new JRadioButton("BLUE");
		Listener lis = new Listener();
		setListener(lis);
		ButtonGroup group = new ButtonGroup();
		group.add(red);
		group.add(green);
		group.add(blue);
		control.add(red);
		control.add(green);
		control.add(blue);
		color.add(control, BorderLayout.SOUTH);
		frame.add(color);
		frame.setVisible(true);
	}

	public void setListener(ActionListener ac) {
		red.addActionListener(ac);
		green.addActionListener(ac);
		blue.addActionListener(ac);
	}

	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getActionCommand() == "RED") {
				color.setBackground(new Color(255, 0, 0));
			} else if (e.getActionCommand() == "GREEN") {
				color.setBackground(new Color(0, 255, 0));
			} else if (e.getActionCommand() == "BLUE") {
				color.setBackground(new Color(0, 0, 255));
			}
		}
	}
}
