import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
public class GUI2 {
	private String setText = "";
	private BankAccount account;
	private JFrame frame;
	private JPanel color;
	private JPanel control;
	private JTextField text;
	private JLabel label;
	private JLabel label2;
	private JTextArea area;
	private JButton with;
	private JButton depos;

	public static void main(String[] args) {
		GUI2 n = new GUI2();
		n.setFrame();
	}

	public void setFrame() {
		frame = new JFrame("Bank Account2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 400);
		text = new JTextField(20);
		label = new JLabel("Amount : ");
		label2 = new JLabel("Balance : ");
		area = new JTextArea(20,35);
		account = new BankAccount(0);
		color = new JPanel(new BorderLayout());
		control = new JPanel(new FlowLayout());
		control.setPreferredSize(new Dimension(100, 50));
		with = new JButton("withdraw");
		depos = new JButton("deposit");
		Listener lis = new Listener();
		setListener(lis);
		control.add(label);
		control.add(text);
		control.add(with);
		control.add(depos);
		control.add(label2);
		control.add(area);
		color.add(control);
		frame.add(color);
		frame.setVisible(true);
	}

	public void setListener(ActionListener ac) {
		with.addActionListener(ac);
		depos.addActionListener(ac);
	}

	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			double amount = Double.parseDouble(text.getText());
			if (e.getActionCommand() == "withdraw") {
				setText +="withdraw : "+ amount+"\n";
				setText +="Balance : "+ account.withdraw(amount)+"\n";
			} else if (e.getActionCommand() == "deposit") {
				setText +="deposit : "+ amount+"\n";
				setText +="Balance : "+ account.deposit(amount)+"\n";
			} 
			area.setText(setText);
		}
	}
}
