
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class ComboBoxV implements ActionListener {
	private JPanel color;
	public static void main(String[] args){
		ComboBoxV n = new ComboBoxV();
		n.setFrame();
	}
	public void setFrame(){
		JFrame frame = new JFrame("COMBO BOX");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300,300);
		
		color= new JPanel(new BorderLayout());
		JPanel control = new JPanel(new FlowLayout());
		control.setPreferredSize(new Dimension(100,50));
		String[] name = {"RED","GREEN","BLUE"};
		JComboBox colorList = new JComboBox(name);
		colorList.setSelectedIndex(2);
		colorList.addActionListener(this);
		control.add(colorList);
		color.add(control,BorderLayout.SOUTH);
		frame.add(color);
		frame.setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JComboBox cb = (JComboBox)e.getSource();
        String name = (String)cb.getSelectedItem();
        setColor(name);
	}
	public void setColor(String n){
		if (n == "RED"){
			color.setBackground(new Color(255,0,0));
		}
		else if (n == "GREEN"){
			color.setBackground(new Color(0,255,0));
		}
		else if(n =="BLUE"){
			color.setBackground(new Color(0,0,255));
		}
	}
	
}
