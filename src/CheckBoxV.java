import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class CheckBoxV {
	private JFrame frame;
	private JPanel control;
	private JPanel color;

	private JCheckBox red;
	private JCheckBox green;
	private JCheckBox blue;

	public static void main(String[] args) {
		CheckBoxV n = new CheckBoxV();
		n.setFrame();
	}

	public void setFrame() {
		frame = new JFrame("RADIO BUTTON");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);

		color = new JPanel(new BorderLayout());
		control = new JPanel(new FlowLayout());
		control.setPreferredSize(new Dimension(100, 50));
		red = new JCheckBox("RED");
		green = new JCheckBox("GREEN");
		blue = new JCheckBox("BLUE");
		Listener lis = new Listener();
		setListener(lis);
		control.add(red);
		control.add(green);
		control.add(blue);
		color.add(control, BorderLayout.SOUTH);
		frame.add(color);
		frame.setVisible(true);
	}

	public void setListener(ActionListener ac) {
		red.addActionListener(ac);
		green.addActionListener(ac);
		blue.addActionListener(ac);
	}

	class Listener implements ActionListener {
		int r=  0;
		int g = 0;
		int b = 0;
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JCheckBox checkbox = (JCheckBox) e.getSource();
			if (checkbox.isSelected()) {
				if (checkbox == red) {
					r += 255;
				} else if (checkbox == green) {
					g += 255;
				} else if (checkbox == blue) {
					b += 255;
				}
			}
			else {
				if (checkbox == red) {
					r -= 255;
				} else if (checkbox == green) {
					g -= 255;
				} else if (checkbox == blue) {
					b -= 255;
				}
			}
			color.setBackground(new Color(r,g,b));
		}
	}
}
