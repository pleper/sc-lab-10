
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class GUI1  {
	private BankAccount account;
	private JFrame frame;
	private JPanel color;
	private JPanel control;
	private JTextField text;
	private JLabel label;
	private JLabel label2;
	
	private JButton with;
	private JButton depos;

	public static void main(String[] args) {
		GUI1 n = new GUI1();
		n.setFrame();
	}

	public void setFrame() {
		frame = new JFrame("Bank Account1");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 100);
		text = new JTextField(20);
		label = new JLabel("Amount : ");
		label2 = new JLabel("Balance : ");
		account = new BankAccount(0);
		color = new JPanel(new BorderLayout());
		control = new JPanel(new FlowLayout());
		control.setPreferredSize(new Dimension(100, 50));
		with = new JButton("withdraw");
		depos = new JButton("deposit");
		Listener lis = new Listener();
		setListener(lis);
		control.add(label);
		control.add(text);
		control.add(with);
		control.add(depos);
		control.add(label2);
		color.add(control);
		frame.add(color);
		frame.setVisible(true);
	}

	public void setListener(ActionListener ac) {
		with.addActionListener(ac);
		depos.addActionListener(ac);
	}

	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			double amount = Double.parseDouble(text.getText());
			if (e.getActionCommand() == "withdraw") {
				label2.setText("balance : "+ account.withdraw(amount));
			} else if (e.getActionCommand() == "deposit") {
				label2.setText("balance : "+ account.deposit(amount));
			} 
		}
	}
	
}
