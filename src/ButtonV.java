import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
public class ButtonV {
	private JFrame frame;
	private JPanel control;
	private JPanel color;
	
	private JButton red;
	private JButton green;
	private JButton blue;
	public static void main(String[] args) {
		ButtonV n = new ButtonV();
		n.setFrame();
	}

	public void setFrame() {
		frame = new JFrame("BUTTON");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);

		color = new JPanel(new BorderLayout());
		control = new JPanel(new FlowLayout());
		control.setPreferredSize(new Dimension(100, 50));
		red = new JButton("RED");
		green = new JButton("GREEN");
		blue = new JButton("BLUE");
		Listener lis = new Listener();
		setListener(lis);
		control.add(red);
		control.add(green);
		control.add(blue);
		color.add(control, BorderLayout.SOUTH);
		frame.add(color);
		frame.setVisible(true);
	}

	public void setListener(ActionListener ac) {
		red.addActionListener(ac);
		green.addActionListener(ac);
		blue.addActionListener(ac);
	}

	class Listener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getActionCommand() == "RED") {
				color.setBackground(new Color(255, 0, 0));
			} else if (e.getActionCommand() == "GREEN") {
				color.setBackground(new Color(0, 255, 0));
			} else if (e.getActionCommand() == "BLUE") {
				color.setBackground(new Color(0, 0, 255));
			}
		}
	}
}
