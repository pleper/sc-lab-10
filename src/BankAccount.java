public class BankAccount {

	private double balance;

	public BankAccount(double initialBalance) {

		balance = initialBalance;

	}

	public double deposit(double amount) {

		balance = balance + amount;
		
		return balance;
	}

	public double withdraw(double amount) {
		balance = balance - amount;
		return balance;
	}

	public double getBalance() {

		return balance;
	}

}
